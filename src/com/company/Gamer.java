package com.company;

import com.company.Field;

/**
 * Created by user on 08.07.2017.
 */
public class Gamer {
    private Field field;
    private int state;

    Gamer(Field field, int state) {
        this.field = field;
        this.state = state;
    }

    public void search() {
        new Thread(new Runnable() {

            @Override
            public void run() {

                for (int i = 0; i < Field.SIZE; i++) {
                    for (int j = 0; j < Field.SIZE; j++) {

                       if (field.getField()[i][j] == 0) {
                           field.set(i, j, state);
                       }
                    }
                }
            }
        }
        ).start();
    }
}
