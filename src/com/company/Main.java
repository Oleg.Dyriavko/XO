package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Field field = new Field();
        Gamer gamer1 = new Gamer(field, 1);
        Gamer gamer2 = new Gamer(field, 2);
        gamer1.search();
        gamer2.search();
    }
}
