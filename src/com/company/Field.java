package com.company;

import java.util.Random;

/**
 * Created by user on 08.07.2017.
 */
public class Field {


    public static final int SIZE = 3;

    private int[][] field;

    public Field() {
        field = new int[SIZE][SIZE];
    }

    public synchronized void set(int x, int y, int state) {
        field[x][y] = state;
        draw();

    }

    private synchronized void draw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(field[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public int[][] getField() {
        return field;
    }
}
